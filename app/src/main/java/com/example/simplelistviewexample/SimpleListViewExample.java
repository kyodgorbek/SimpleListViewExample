package com.example.simplelistviewexample;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SimpleListViewExample extends ListActivity {

    String[] listViewItems = {"Android ListView Example", "Java Programming", "Android Material Design",
            "Simple ListView Example", "ListView Example/Tutorial", "Android ListView Example",
            "Java Programming", "Android Material Design", "Simple ListView Example",
            "ListView Example/Tutorial", "Android ListView Example", "Java Programming",
            "Android Material Design", "Simple ListView Example",
            "ListView Example/Tutorial"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list_view_example);

        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listViewItems));
        @Override
        protected void onListItemClick(ListView ListView l;
        l, View View v;
        v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            Toast.makeText(this, "You have selected : " + listViewItems[position], Toast.LENGTH_LONG).show();
        }
    }



